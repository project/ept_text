<!-- @codingStandardsIgnoreFile -->
# Extra Block Types (EPT): Text

Extra Block Types: Text module provides ability to add Text and Title with
WYSIWYG editor.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/ept_text).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/ept_text).


## Table of contents

- Requirements
- Recommended modules
- Installation
- Configuration
- Troubleshooting
- FAQ
- Maintainers


## Requirements

This project requires the following items:

- [EPT Core](https://www.drupal.org/project/ept_core)
- [Paragraphs](https://www.drupal.org/project/paragraphs)

EPT Modules use Media module with Media Image type for background images.
Check Media Image type exists before install EPT Core module.


## Recommended modules

EPT modules provide ability to add different blocks in Layout Builder in few
clicks.
You can install separate block types from this bunch of EPT projects:

- [EPT Accordion / FAQ](https://www.drupal.org/project/ept_accordion)
- [EPT Basic Button](https://www.drupal.org/project/ept_basic_button)
- [EPT Bootstrap Button](https://www.drupal.org/project/ept_bootstrap_button)
- [EPT Call to Action](https://www.drupal.org/project/ept_cta)
- [EPT Carousel](https://www.drupal.org/project/ept_carousel)
- [EPT Counter](https://www.drupal.org/project/ept_counter)
- [EPT Quote](https://www.drupal.org/project/ept_quote)
- [EPT Slick Slider](https://www.drupal.org/project/ept_slick_slider)
- [EPT Slideshow](https://www.drupal.org/project/ept_slideshow)
- [EPT Stats](https://www.drupal.org/project/ept_stats)
- [EPT Tabs](https://www.drupal.org/project/ept_tabs)
- [EPT Text](https://www.drupal.org/project/ept_text)
- [EPT Timeline](https://www.drupal.org/project/ept_timeline)
- [EPT Webform](https://www.drupal.org/project/ept_webform)
- [EPT Webform Popup](https://www.drupal.org/project/ept_webform_popup)

More about EPT blocks read on EPT Core module page:
[EPT Core](https://www.drupal.org/project/ept_core)

If you want to enhance UI for adding blocks from Layout builder try this module:
[Layout Builder Modal](https://www.drupal.org/project/layout_builder_modal)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration


After enable this project will be available a new block type called `EPT Text`

You can go at Structure » Block layout » Add custom block » EPT Text

When you're creating this new custom block, you can see that there is a new Tab
called `Settings` and there you can configure the Design options inside of
`Block Settings` tab

You can configure there:
- Margin;
- Border;
- Padding;
- Border Color;
- Border Style;
- Border Radius;
- Background Color;
- Background Image Style;
- Edge to Edge;
- Container Max Width.

After create your custom block with block type `EPT Text` you can place this
block using Layout Builder. In the front-end will be able to see the design
options applied in the front-end.

EPT Core has configuration form with Primary/Secondary colors and
Mobile/Tablet/Desktop breakpoints in:
Administration » Configuration » Content authoring » Extra Block Types (EPT)
settings

These configs will be applied to other EPT blocks by default.

## Troubleshooting

- If you see the error during EPT modules installation: "The EPT Carousel needs
  to be created "Image" media type. (Currently using Media type Image version
  Not created)"
  Then you need to create Image media type:
  Structure » Media types » Add media type

- If you use Field Layout module, it will automatically apply Layout Builder for
  new EPT block types. So you will need to disable Layout Builder for displaying
  block type fields:
  /admin/structure/block/block-content/manage/ept_text/display/default


## FAQ

**Q: Can I use only one EPT block type, for example EPT Slideshow, without other modules?**

**A:** Yes, sure. EPT block types tend to be standalone modules. You can install
only needed block types.


## Maintainers

- Ivan Abramenko - [levmyshkin](https://www.drupal.org/u/levmyshkin)
- Narine Tsaturyan - [Narine_Tsaturyan](https://www.drupal.org/u/narine_tsaturyan)
